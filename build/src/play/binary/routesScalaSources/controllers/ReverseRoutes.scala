
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/HOME/TUM privat/SocialGameInstGuide/Assignment2_Server/conf/routes
// @DATE:Sun Jul 01 21:47:50 CEST 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:6
package controllers {

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def updateUserLocation(firebaseId:String, longitude:Double, latitude:Double): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "position/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseId", firebaseId)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Double]].unbind("longitude", longitude)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Double]].unbind("latitude", latitude)) + "/update")
    }
  
    // @LINE:30
    def getPostionOfFriends(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "position/getPostionOfFriends")
    }
  
    // @LINE:23
    def getGetPunishList(firebaseId:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "punish/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseId", firebaseId)) + "/getGetPunishList")
    }
  
    // @LINE:17
    def checkID(firebaseId:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "friend/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseId", firebaseId)) + "/checkID")
    }
  
    // @LINE:29
    def getPostionOfUser(firebaseId:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "position/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseId", firebaseId)) + "/getPostionOfUser")
    }
  
    // @LINE:14
    def dbInit(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "init/init")
    }
  
    // @LINE:19
    def addFriend(firebaseIdOwn:String, fireBaseIdFriend:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "friend/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseIdOwn", firebaseIdOwn)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("fireBaseIdFriend", fireBaseIdFriend)) + "/addFriend")
    }
  
    // @LINE:18
    def checkIfFriend(firebaseIdOwn:String, fireBaseIdFriend:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "friend/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseIdOwn", firebaseIdOwn)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("fireBaseIdFriend", fireBaseIdFriend)) + "/checkIfFriend")
    }
  
    // @LINE:33
    def updateEvents(name:String, longitude:Double, latitude:Double, strafe1:String, strafe2:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "event/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("name", name)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Double]].unbind("longitude", longitude)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Double]].unbind("latitude", latitude)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("strafe1", strafe1)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("strafe2", strafe2)) + "/update")
    }
  
    // @LINE:24
    def getOwerPunishList(firebaseId:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "punish/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseId", firebaseId)) + "/getOwerPunishList")
    }
  
    // @LINE:22
    def pushPunishIntoListG(firebaseIdOwn:String, firebaseIdOther:String, punishment:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "punish/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseIdOwn", firebaseIdOwn)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("firebaseIdOther", firebaseIdOther)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("punishment", punishment)) + "/pushPunishIntoListG")
    }
  
    // @LINE:10
    def test(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "test")
    }
  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:9
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }


}
