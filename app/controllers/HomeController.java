package controllers;

import play.mvc.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.*;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import play.Logger;



import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    @Inject
    private UserLocationsRepository locations;
    @Inject
    private EventLocationsRepository events;
    @Inject
    private Gamerepository game;



    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }

    public Result test(){
        return ok("Got Request");
    }

    public Result addPunishment(String uuid, String punishment, int pos)
    {
        Logger.info("add punishment to game");
        //Logger.info(""+ownId);
        //Logger.info(""+othId);

        String decodepunishment = null;
        try {
            decodepunishment = URLDecoder.decode(punishment, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //Logger.info(""+decodepunishment);

        game.addPunishment(uuid,decodepunishment,pos);

        return ok("Worked");

    }

    public Result getOwerPunishList(String oid)
    {
        Logger.info("get the punishment list you owe");

        Result ret;

        UserLocation usr = locations.getThePunishmentListYouOwe(oid);

        //Logger.info(""+usr.getpunishment);

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(PunishObject pun : usr.owepunishment)
        {
            ObjectNode punishNode = Json.newObject();
            punishNode.put("user",pun.user.toString());
            punishNode.put("punishment",pun.punishment);
            listArray.add(punishNode);
        }

        searchResult.put("AllOweDebts",listArray);

        ret = ok(searchResult);

        return ret;
    }

    public Result getGetPunishList(String oid)
    {
        Logger.info("get the punishment list");

        Result ret;

        UserLocation usr = locations.getThePunishmentList(oid);

        //Logger.info(""+usr.getpunishment);

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(PunishObject pun : usr.getpunishment)
        {
            ObjectNode punishNode = Json.newObject();
            punishNode.put("user",pun.user.toString());
            punishNode.put("punishment",pun.punishment);
            listArray.add(punishNode);
        }

        searchResult.put("AllGetDebts",listArray);

        ret = ok(searchResult);

        return ret;
    }

    public Result pullPunishFromList(String ownId, String othId, String punishment)
    {
        Logger.info("pull punishment from the punishlist");

        String decodepunishment = null;
        try {
            decodepunishment = URLDecoder.decode(punishment, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        locations.pullOwePunishment(othId,new String[]{ownId,decodepunishment});
        locations.pullGetPunishment(ownId,new String[]{othId,decodepunishment});

        return ok("Worked");
    }

    public Result pushPunishIntoListG(String ownId, String othId, String punishment)
    {
        Logger.info("push a punishment into the punishlist");
        //Logger.info(""+ownId);
        //Logger.info(""+othId);

        String decodepunishment = null;
        try {
            decodepunishment = URLDecoder.decode(punishment, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //Logger.info(""+decodepunishment);

        locations.addGetPunishment(othId,new String[]{ownId,decodepunishment});

        return ok("Worked");
    }

    public Result addFriend(String oid, String fid)
    {
        Logger.info("update friends");
        locations.addFriend(oid,fid);
        return ok("Worked");
    }

    public Result checkID(String id)
    {
        Logger.info("check if id is in DB");

        Result ret;

        UserLocation loc = locations.getIDofID(id);
        ObjectNode searchResult = Json.newObject();

        if(loc!=null)
            searchResult.put("answer", "true");


        ret = ok(searchResult);
        return ret;
    }

    public Result checkIfFriend(String owndid, String friendid)
    {
        Logger.info("check if id is a Friend");

        Result ret;

        UserLocation loc = locations.getIDifFriend(owndid,friendid);
        ObjectNode searchResult = Json.newObject();

        if(loc!=null)
            searchResult.put("answer", "true");


        ret = ok(searchResult);
        return ret;
    }


    public Result updateUserLocation(String id, Double longitude, Double latitude){
        Logger.info("update Location");

        UserLocation loc = locations.getLocation(id);

        if(loc!=null){
            //update Location
            Logger.info("update User");
            loc.updateLocation(new Double[]{latitude, longitude});
            locations.update(loc);
        }else{
            Logger.info("new User");
            //create Location
            loc = new UserLocation(id, new Double[]{latitude, longitude}, new String[]{}, new ArrayList<PunishObject>(), new ArrayList<PunishObject>());
            locations.insert(loc);
        }

        return ok("Worked");
    }

    public Result updateEvents(String name, Double longitude, Double latitude, String strafe1, String strafe2){
        Logger.info("update Events");

        return ok("Worked");
    }

    public Result dbInit()
    {
        Logger.info("Fill Db with dummy data");

        Result ret;


        events.drop();

        String userID1 = "FHEVC0X3ThYnhoyXF5SXrg2YU4K2";
        Double[] loc1 = new Double[]{10.45,48.46};
        String userID2 = "T3NrLM76I0VpSoW9mfAlIU4kZnq1";
        Double[] loc2 = new Double[]{11.45,48.46};


        String eid1 = "1";
        String ename1 = "TUM Informatik";
        Double[] eloc1 = new Double[]{11.668275000000001,48.366499999999995};
        String estrafe11 = "Fahr mit einem anderen Verkehrsmittel zur TUM.";
        int flag1 = 0;
        String estrafe12 = "Lies ein beliebeges Skript nochmal komplett durch.";
        int flag2 = 0;


        events.fillEvent(new EventLocation(eid1,ename1,eloc1,estrafe11,flag1,estrafe12,flag2));

        return ok("Worked");

    }



    public Result getPostionOfFriends(){
        Logger.info("Getting Position of Friends");

        Result ret;

        Iterable<UserLocation> friends = locations.getLocationsOfFriends();

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(UserLocation usr : friends) {

            ObjectNode locationNode = Json.newObject();

            locationNode.put("user",usr.id.toString());

            locationNode.put("loc1",usr.loc1[0]);
            locationNode.put("loc12",usr.loc1[1]);

            listArray.add(locationNode);

        }
        searchResult.put("AllFriends",listArray);
        //Logger.info(searchResult.toString());
        ret = ok(searchResult);
        return  ret;

    }

    public Result getPostionOfUser(String id){
        Logger.info("Getting Position of User");

        Result ret;

        UserLocation location = locations.getLocation(id);

        //Get All Locations

        ObjectNode searchResult = Json.newObject();
        ArrayNode locationArray = searchResult.arrayNode();

        ObjectNode locationNode = Json.newObject();

        locationNode.put("user",location.id);

        locationNode.put("loc1",location.loc1[0]);
        locationNode.put("loc12",location.loc1[1]);

       /*
        if(location.loc1 !=null){
            Logger.info("loc1 "+location.loc1[0]+","+location.loc1[1]);
            locationNode.put("loc1",location.loc1[0]);
            locationNode.put("loc12",location.loc1[1]);
        } else{
            Logger.info("null1");
            locationNode.put("loc1"," ");
            locationNode.put("loc12"," ");
        }
        if(location.loc2 != null){
            Logger.info("loc2");
            Logger.info("loc2 "+location.loc2[0]+","+location.loc2[1]);
            locationNode.put("loc2",location.loc2[0]);
            locationNode.put("loc22",location.loc2[1]);
        } else{
            Logger.info("null2");
            locationNode.put("loc2"," ");
            locationNode.put("loc22"," ");
        }
        if(location.loc3 !=null){
            Logger.info("loc3");
            locationNode.put("loc3",location.loc3[0]);
            locationNode.put("loc32",location.loc3[1]);
        } else {
            Logger.info("null3");
            locationNode.put("loc3"," ");
            locationNode.put("loc32"," ");
        }
        */
        locationArray.add(locationNode);

        searchResult.put("locations", locationArray);


        ret = ok(searchResult);

        return ret;
    }

    public Result getPositionsAround(double lon, double lat){
        Logger.info("Getting Position of Players Around");

        Result ret;

        Iterable<UserLocation> players = locations.getLocationsOfPlayersAround(lon, lat);

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(UserLocation usr : players) {

            ObjectNode locationNode = Json.newObject();

            locationNode.put("user",usr.id.toString());

            locationNode.put("loc1",usr.loc1[0]);
            locationNode.put("loc12",usr.loc1[1]);

            listArray.add(locationNode);
            searchResult.put("PlayersAround",listArray);

        }
        //Logger.info(searchResult.toString());
        ret = ok(searchResult);
        return  ret;

    }

    public Result getEvent(double lon,double lat){
        Logger.info("Getting Events Around");

        Result ret;

        Logger.info(""+lon+","+lat);
        Iterable<EventLocation> eventss = events.getEventsAround(lon, lat);

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(EventLocation vnt : eventss) {

            ObjectNode locationNode = Json.newObject();

            locationNode.put("event",vnt.id.toString());

            locationNode.put("loc1",vnt.loc[0]);
            locationNode.put("loc2",vnt.loc[1]);

            locationNode.put("strafe1",vnt.strafe1);
            locationNode.put("flag1",vnt.flag1);
            locationNode.put("strafe2",vnt.strafe2);
            locationNode.put("flag2",vnt.flag2);

            listArray.add(locationNode);
            searchResult.put("EventsAround",listArray);

        }
        //Logger.info(searchResult.toString());
        ret = ok(searchResult);
        return  ret;
    }

    public Result userLeft(String id)
    {
        Logger.info("User Left");
        game.userLeft(id);
        return ok("Worked");
    }

    public Result createGame(String id, String name, double lon, double lat)
    {
        Logger.info("Game created");

        ArrayList<String> s = new ArrayList<String>();
        s.add(name);
        Double[] d = new Double[2];
        d[0] = lon;
        d[1] = lat;
        Game g = new Game(id, d , s, "", 0, true, false, 1, 0);
        game.insert(g);
        return ok("Worked");
    }

    public Result getJoined(String id){
        Logger.info("Getting Joined Players");

        Result ret;

        Game g = game.getJoined(id);

        Logger.info(g.id);

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(String str : g.names)
        {
            ObjectNode nameNode = Json.newObject();
            nameNode.put("name",str);
            listArray.add(nameNode);
        }

        searchResult.put("allJoined",listArray);

        ret = ok(searchResult);
        Logger.info("Returning Joined Players");
        return ret;
    }

	public Result getGamesAround(double lo, double la){
		Result ret;

        Iterable<Game> games = game.getGamesAround(lo,la);

        //Logger.info(g.id);

        ObjectNode searchResult = Json.newObject();
        ArrayNode listArray = searchResult.arrayNode();

        for(Game g : games)
        {
            Logger.info(g.id);

            ObjectNode perGame = Json.newObject();
            perGame.put("GameID",g.id);
            perGame.put("started", g.started);
            Logger.info("hi\n"+g.started);
            ArrayNode parts = perGame.arrayNode();
            perGame.put("names",parts);


            for(String str : g.names)
            {
                ObjectNode nameNode = Json.newObject();
                nameNode.put("name",str);
                parts.add(nameNode);
            }


            //ObjectNode nameNode = Json.newObject();
            //nameNode.put("name",str);
            listArray.add(perGame);
        }

        searchResult.put("GamesPlusParts",listArray);
        Logger.info(""+searchResult.toString());

        ret = ok(searchResult);
        return ret;


    }

    public Result pushPartIntoGame(String gId, String id)
    {
        Logger.info("Push id into Game.");

        Result ret;

        game.pushPartIntoGame(gId,id);

        ret = ok("Worked");
        return ret;
    }

    public Result getVotes(String id){
        Logger.info("Getting Votes");

        Result ret;

        Game g = game.getGame(id);

        ObjectNode searchResult = Json.newObject();
        //ArrayNode listArray = searchResult.arrayNode();


        ObjectNode locationNode = Json.newObject();

        locationNode.put("parts",g.parts);

        locationNode.put("votecount",g.votecount);

       // listArray.add(locationNode);
        searchResult.put("votes",locationNode);

        //Logger.info(searchResult.toString());
        ret = ok(searchResult);

        Logger.info("Getting Votes");
        return  ret;
    }

    public Result getPunish(String id){
        Logger.info("Getting Votes");

        Result ret;

        Game g = game.getGame(id);

        ObjectNode searchResult = Json.newObject();
        //ArrayNode listArray = searchResult.arrayNode();


        ObjectNode locationNode = Json.newObject();

        locationNode.put("punish",g.strafe);

        // listArray.add(locationNode);
        searchResult.put("Strafe",locationNode);

        //Logger.info(searchResult.toString());
        ret = ok(searchResult);

        Logger.info("Getting Votes");
        return  ret;
    }

    public Result finishVote(String id){
        game.finishVote(id);
        return ok("Worked");
    }

    public Result start(String id){
        game.start(id);
        return ok("Worked");
    }

    public Result close(String id){
        game.close(id);
        return ok("Worked");
    }

    public Result checkLeft(String id){

        Result ret;

        Game g = game.checkLeft(id);

        ObjectNode searchResult = Json.newObject();
        searchResult.put("running",g.stillRunning);

        ret = ok(searchResult);
        return  ret;
    }

    public Result voteForFinish(String id)
    {
        Logger.info("Voted");
        game.voteForFinish(id);
        return ok("Worked");
    }

/*
    public Result getAllLocationsTest(String id){


        Logger.info("getting All Locations");

        Result ret;

        //LAT /Long
        Location location = new Location("Test", new Double[]{48.218800, 11.624707}); //Allianz Arena
        location.updateLocation(new Double[]{48.149101, 11.567317}); //TUM Stammgelände
        location.updateLocation(new Double[]{48.262617, 11.668276}); //TUM Informatik

        //Get All Locations

        ObjectNode searchResult = Json.newObject();
        ArrayNode locationArray = searchResult.arrayNode();

        ObjectNode locationNode = Json.newObject();

        locationNode.put("user",location.id);
        locationNode.put("loc1",location.loc1[0]);
        locationNode.put("loc12",location.loc1[1]);
        locationNode.put("loc2",location.loc2[0]);
        locationNode.put("loc22",location.loc2[1]);
        locationNode.put("loc3",location.loc3[0]);
        locationNode.put("loc32",location.loc3[1]);

        locationArray.add(locationNode);

        searchResult.put("locations", locationArray);

        ret = ok(searchResult);

        return ret;
    }
 */
/*public Result getGame(double lon,double lat){
    Logger.info("Getting Games Around");

    Result ret;

    Iterable<GameLocation> gamess = games.getGamesAround(lon, lat);

    ObjectNode searchResult = Json.newObject();
    ArrayNode listArray = searchResult.arrayNode();

    for(GameLocation gms : gamess) {

        ObjectNode locationNode = Json.newObject();
        locationNode.put("game",gms.id.toString());
        locationNode.put("loc1",gms.loc[0]);
        locationNode.put("loc2",gms.loc[1]);
        listArray.add(locationNode);
        //TODO
        searchResult.put("GamesAround",listArray);

    }
    ret = ok(searchResult);
    return  ret;
}*/
//TODO Join Game related methods




}
