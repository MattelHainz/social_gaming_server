package models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.util.Map;


public class EventLocation {

    @JsonProperty("_id")
    public String id;

    //Location
    public String name;
    public Double[] loc;


    // FLAG bezeichnet den Zeugen: 0 kein Zeuge, 1 ein Zeuge, 2 alle als Zeugen.
    public String strafe1;
    public int flag1;
    public String strafe2;
    public int flag2;



    public EventLocation(){

    }

    public EventLocation(String id, String name, Double[] loc, String strafe1, int flag1, String strafe2, int flag2){
        this.id = id;
        this.name = name;
        this.loc = loc;
        this.strafe1 = strafe1;
        this.flag1 = flag1;
        this.strafe2 = strafe2;
        this.flag2 = flag2;
    }
}