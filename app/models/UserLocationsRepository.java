package models;

import java.lang.*;
import java.util.List;

import org.jongo.MongoCollection;


import javax.inject.*;

import uk.co.panaxiom.playjongo.*;
import play.Logger;

import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.MongoClientURI;

import com.google.common.collect.Lists;
import java.util.Map;
import java.util.ArrayList;

@Singleton
public class UserLocationsRepository {

    @Inject
    private PlayJongo jongo;

    private static UserLocationsRepository instance = null;

    public UserLocationsRepository()
    {
        instance = this;
    }

    public static UserLocationsRepository getInstance(){
        return instance;
    }

    public MongoCollection locations() {
        //MongoClient mongoClient = new MongoClient();
        //DB db = new MongoClient().getDB("AndroidTutorial");
        MongoCollection locationCollection =  jongo.getCollection("UserLocation");

        // make sure we use 2d indices on a sphere to use geospatial queries
        locationCollection.ensureIndex("{loc: '2dsphere'}");
        return locationCollection;
    }

    public UserLocation getThePunishmentListYouOwe(String oid)
    {
        Logger.info("execute get owe-punishments");

        return locations().findOne("{_id: #}",oid).as(UserLocation.class);
    }

    public UserLocation getThePunishmentList(String oid) {
        Logger.info("execute get punishments");

        //Logger.info(""+ul.getpunishment.get(0).from+" "+ul.getpunishment.get(0).punishment);

        return locations().findOne("{_id: #}",oid).as(UserLocation.class);
    }


    public void pullOwePunishment(String id, String[] p)
    {
        Logger.info("exeOwe: "+id+" user: "+p[0]+" punishment:"+p[1]);
        locations().update("{_id: #}",id).with("{$pull:{owepunishment: {user: #, punishment: #}, {multi: true}}}",p[0],p[1].trim());
    }

    public void pullGetPunishment(String id, String[] p)
    {
        Logger.info("exeGet: "+id+" user: "+p[0]+" punishment:"+p[1]);
        locations().update("{_id: #}",id).with("{$pull:{getpunishment: {user: #, punishment: #}, {multi: true}}",p[0],p[1].trim());
    }

    public void addGetPunishment(String othid, String[] p)
    {
        Logger.info("execute add punishment query "+othid);
        locations().update("{_id: #}",othid).with("{$push:{getpunishment: {user: #, punishment: #}}}",p[0],p[1]);
        locations().update("{_id: #}",p[0]).with("{$push:{owepunishment: {user: #, punishment: #}}}",othid,p[1]);

        //locations().update("{_id: #}",othid).with("{$pull:{getpunishment: {user: #, punishment: #}}}",p[0],p[1]);



        //locations().update("{_id: #},{$unset:{getpunishment.1: 1}}",othid);
        //locations().update("{_id: #}",othid).with("{$pull:{getpunishment: null }}");



        //db.collection.update({}, {$unset : {[arrIndex] : 1 }});
        //db.collection.update({}, {$unset : {"array.2" : 1 }});
        //db.collection.update({}, {$pull : {"array" : null }});


    }

    public void addFriend(String oid, String fid)
    {
        Logger.info("execute add a new friend query");
        locations().update("{_id: #}",oid).with("{$addToSet:{friends: #}}",fid);
        locations().update("{_id: #}",fid).with("{$addToSet:{friends: #}}",oid);

    }

    public UserLocation getIDifFriend(String oid, String fid) {
        Logger.info("execute check if friend query");
        return locations().findOne("{$and: [{_id: #},{ friends: { $in: [ # ] }}]}",oid,fid).as(UserLocation.class);
    }

    public UserLocation getIDofID(String id){

        Logger.info("execute check id query");
        return locations().findOne("{_id: #}",id).as(UserLocation.class);
    }

    public UserLocation getLocation(String id){
        Logger.info(locations().find().toString());
        return locations().findOne("{_id: #}",id).as(UserLocation.class);
    }

    public Iterable<UserLocation> getLocationsOfFriends(){
        //Logger.info(locations().find().as(UserLocation.class).toString());

        return locations().find().as(UserLocation.class);
    }


    public void insert(UserLocation l) {
        locations().save(l);
    }

    public void update(UserLocation location) {
        // copy the user to be sure that database IDs will be taken care of
        locations().update("{_id: #}",location.id).with(copyLocation(location));
    }

    private static UserLocation copyLocation(UserLocation location) {
        UserLocation copy = new UserLocation();

        copy.id = location.id;
        copy.loc1 = location.loc1;
        //copy.loc2 = location.loc2;
        //copy.loc3 = location.loc3;

        return copy;
    }

    public Iterable<UserLocation> getLocationsOfPlayersAround(double lon, double lat){

        return locations().find("{loc: { $geoWithin: { $centerSphere: [ [  # , # ], 0.05/3963.2 ] } }}",lon, lat ).as(UserLocation.class);
    }




}