package models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.util.Map;
import java.util.ArrayList;


public class Game {

    @JsonProperty("_id")
    public String id;

    public Double[] loc;

    public ArrayList<String> names;

    public String strafe;

    // 0, 1 2 siehe EventLocation.
    public int flag;

    public boolean started;

    @JsonProperty("_stillRunning")
    public boolean stillRunning;

    public int parts;

    public int votecount;

    public Game(){

    }

    public Game(String id, Double[] loc , ArrayList<String> names, String strafe, int flag, boolean started, boolean stillRunning, int parts, int votecount ){
        this.id = id;
        this.loc = loc;
        this.names = names;
        this.strafe = strafe;
        this.flag = flag;
        this.started = started;
        this.stillRunning = stillRunning;
        this.parts = parts;
        this.parts = parts;
        this.votecount = votecount;
    }
}