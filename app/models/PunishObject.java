package models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PunishObject {

    @JsonProperty("user")
    public String user;
    @JsonProperty("punishment")
    public String punishment;

    public PunishObject(){

    }

    public PunishObject( String o,   String t){
        this.user = o;
        this.punishment = t;

    }
}