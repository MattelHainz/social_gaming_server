package models;

import java.lang.*;
import java.util.List;

import org.jongo.MongoCollection;


import javax.inject.*;

import uk.co.panaxiom.playjongo.*;
import play.Logger;

import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.MongoClientURI;

import com.google.common.collect.Lists;
import java.util.Map;
import java.util.ArrayList;

@Singleton
public class EventLocationsRepository {

    @Inject
    private PlayJongo jongo;

    private static EventLocationsRepository instance = null;

    public EventLocationsRepository()
    {
        instance = this;
    }

    public static EventLocationsRepository getInstance(){
        return instance;
    }

    public MongoCollection events() {
        //MongoClient mongoClient = new MongoClient();
        //DB db = new MongoClient().getDB("AndroidTutorial");
        MongoCollection eventLocationCollection =  jongo.getCollection("EventLocation");

        // make sure we use 2d indices on a sphere to use geospatial queries
        eventLocationCollection.ensureIndex("{loc: '2dsphere'}");
        return eventLocationCollection;
    }

    public void drop(){
        events().drop();
    }

    public void fillEvent(EventLocation l){
        events().save(l);
    }

    public Iterable<EventLocation> getEventsAround(double lon, double lat){
        //double i = 0.05/3963.2;
        //lon = lon%90;
        //lat = lat%90;
        return events().find("{loc: { $geoWithin: { $centerSphere: [ [  # , # ], 0.0000126 ] } }}" , lon, lat ).as(EventLocation.class);
        //return events().find().as(EventLocation.class);
    }

}