package models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.util.Map;
import java.util.ArrayList;

public class UserLocation {

    @JsonProperty("_id")
    public String id;

    //Last Location
    public Double[] loc1;
    public String[] friends;

    @JsonProperty("getpunishment")
    public ArrayList<PunishObject> getpunishment;

    @JsonProperty("owepunishment")
    public ArrayList<PunishObject> owepunishment;

    //public Double[] loc2;

    //public Double[] loc3;

    public UserLocation(){

    }

    public UserLocation( String id, Double[] loc,  String[] friends, ArrayList<PunishObject> g, ArrayList<PunishObject> o){
        this.id = id;
        this.loc1 = loc;
        this.friends = friends;
        this.getpunishment = g;
        this.owepunishment = o;
    }

    public void updateLocation(Double[] loc){
        /*this.loc3[0] = loc2[0];
        this.loc3[0] = loc2[1];
        this.loc2[0] = loc1[0];
        this.loc2[0] = loc1[1];
        this.loc1[0] = loc2[0];
        this.loc1[0] = loc2[1];*/
        //this.loc3 = loc2;
        //this.loc2 = loc1;
        this.loc1 = loc;
    }

}
