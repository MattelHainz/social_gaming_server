package models;

import java.lang.*;
import java.util.List;

import org.jongo.MongoCollection;


import javax.inject.*;

import uk.co.panaxiom.playjongo.*;
import play.Logger;

import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.MongoClientURI;

import com.google.common.collect.Lists;
import java.util.Map;
import java.util.ArrayList;

@Singleton
public class Gamerepository {

    @Inject
    private PlayJongo jongo;

    private static Gamerepository instance = null;

    public Gamerepository()
    {
        instance = this;
    }

    public static Gamerepository getInstance(){
        return instance;
    }

    public MongoCollection games() {
        //MongoClient mongoClient = new MongoClient();
        //DB db = new MongoClient().getDB("AndroidTutorial");
        MongoCollection gameCollection =  jongo.getCollection("Game");

        // make sure we use 2d indices on a sphere to use geospatial queries
        gameCollection.ensureIndex("{loc: '2dsphere'}");
        return gameCollection;
    }


    public void userLeft(String id){
        games().update("{ _id: #}",id).with("{ $set:{ _stillRunning: false }}");
    }

    public Iterable<Game> getGamesAround(double lo, double la){
        Logger.info("execute get games around");

        //Logger.info(""+ul.getpunishment.get(0).from+" "+ul.getpunishment.get(0).punishment);

        return games().find("{loc: { $geoWithin: { $centerSphere: [ [  # , # ], 0.0000126 ] } }}",lo, la ).as(Game.class);
    }

    public void insert(Game g) {
        games().save(g);
    }

    public void finishVote(String id){
        //try {
           //games().remove("{_id: #}", id);
        //}catch(Exception e){ print(e);}
    }

    public Game getJoined(String id) {
        Logger.info("execute get joined");

        return games().findOne("{_id: #}",id).as(Game.class);
    }

    public Game checkLeft(String id) {
        return games().findOne("{_id: #}",id).as(Game.class);
    }

    public void pushPartIntoGame(String gid, String id)
    {
        games().update("{_id: #}",gid).with("{$addToSet:{names: #}}",id);
        games().update("{ _id: #}",gid).with("{ $inc: { parts: 1} }");
    }
    public Game getGame(String id){
        return games().findOne("{_id: #}",id).as(Game.class);
    }

    public void voteForFinish(String id){
        //games().update("{_id: #}", id).with({ $inc: {"_votecount: 1");
        Logger.info("incrementing votecount");
        games().update("{ _id: #}",id).with("{ $inc: { votecount: 1} }");

    }

    public void addPunishment(String id, String Punishment, int pos){
        games().update("{ _id: #}",id).with("{$set: {strafe: #, flag: #}}", Punishment,pos);
    }

    public void start(String id){
        games().update("{ _id: #}",id).with("{$set: {_stillRunning: true}}");
    }

    public void close(String id){
        games().update("{ _id: #}",id).with("{$set: {started: false}}");
    }



}