
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/HOME/TUM privat/SocialGameInstGuide/Assignment2_Server/conf/routes
// @DATE:Sun Jul 01 23:11:43 CEST 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
