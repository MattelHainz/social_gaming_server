
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/HOME/TUM privat/SocialGameInstGuide/Assignment2_Server/conf/routes
// @DATE:Sun Jul 01 23:11:43 CEST 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_0: controllers.HomeController,
  // @LINE:9
  Assets_1: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_0: controllers.HomeController,
    // @LINE:9
    Assets_1: controllers.Assets
  ) = this(errorHandler, HomeController_0, Assets_1, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, Assets_1, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """test""", """controllers.HomeController.test()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """init/init""", """controllers.HomeController.dbInit()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """friend/""" + "$" + """firebaseId<[^/]+>/checkID""", """controllers.HomeController.checkID(firebaseId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """friend/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """fireBaseIdFriend<[^/]+>/checkIfFriend""", """controllers.HomeController.checkIfFriend(firebaseIdOwn:String, fireBaseIdFriend:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """friend/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """fireBaseIdFriend<[^/]+>/addFriend""", """controllers.HomeController.addFriend(firebaseIdOwn:String, fireBaseIdFriend:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """punish/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """firebaseIdOther<[^/]+>/""" + "$" + """punishment<[^/]+>/pushPunishIntoListG""", """controllers.HomeController.pushPunishIntoListG(firebaseIdOwn:String, firebaseIdOther:String, punishment:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """punish/""" + "$" + """firebaseId<[^/]+>/getGetPunishList""", """controllers.HomeController.getGetPunishList(firebaseId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """punish/""" + "$" + """firebaseId<[^/]+>/getOwerPunishList""", """controllers.HomeController.getOwerPunishList(firebaseId:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """punish/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """firebaseIdOther<[^/]+>/""" + "$" + """punishment<[^/]+>/pullPunishFromList""", """controllers.HomeController.pullPunishFromList(firebaseIdOwn:String, firebaseIdOther:String, punishment:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """position/""" + "$" + """firebaseId<[^/]+>/""" + "$" + """longitude<[^/]+>/""" + "$" + """latitude<[^/]+>/update""", """controllers.HomeController.updateUserLocation(firebaseId:String, longitude:Double, latitude:Double)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """position/""" + "$" + """firebaseId<[^/]+>/getPostionOfUser""", """controllers.HomeController.getPostionOfUser(firebaseId:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """position/getPostionOfFriends""", """controllers.HomeController.getPostionOfFriends()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """event/""" + "$" + """name<[^/]+>/""" + "$" + """longitude<[^/]+>/""" + "$" + """latitude<[^/]+>/""" + "$" + """strafe1<[^/]+>/""" + "$" + """strafe2<[^/]+>/update""", """controllers.HomeController.updateEvents(name:String, longitude:Double, latitude:Double, strafe1:String, strafe2:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Assets_versioned1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned1_invoker = createInvoker(
    Assets_1.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_HomeController_test2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("test")))
  )
  private[this] lazy val controllers_HomeController_test2_invoker = createInvoker(
    HomeController_0.test(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "test",
      Nil,
      "GET",
      this.prefix + """test""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_HomeController_dbInit3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("init/init")))
  )
  private[this] lazy val controllers_HomeController_dbInit3_invoker = createInvoker(
    HomeController_0.dbInit(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "dbInit",
      Nil,
      "POST",
      this.prefix + """init/init""",
      """ dbInit""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_HomeController_checkID4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("friend/"), DynamicPart("firebaseId", """[^/]+""",true), StaticPart("/checkID")))
  )
  private[this] lazy val controllers_HomeController_checkID4_invoker = createInvoker(
    HomeController_0.checkID(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "checkID",
      Seq(classOf[String]),
      "GET",
      this.prefix + """friend/""" + "$" + """firebaseId<[^/]+>/checkID""",
      """ friends""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_HomeController_checkIfFriend5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("friend/"), DynamicPart("firebaseIdOwn", """[^/]+""",true), StaticPart("/"), DynamicPart("fireBaseIdFriend", """[^/]+""",true), StaticPart("/checkIfFriend")))
  )
  private[this] lazy val controllers_HomeController_checkIfFriend5_invoker = createInvoker(
    HomeController_0.checkIfFriend(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "checkIfFriend",
      Seq(classOf[String], classOf[String]),
      "GET",
      this.prefix + """friend/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """fireBaseIdFriend<[^/]+>/checkIfFriend""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_HomeController_addFriend6_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("friend/"), DynamicPart("firebaseIdOwn", """[^/]+""",true), StaticPart("/"), DynamicPart("fireBaseIdFriend", """[^/]+""",true), StaticPart("/addFriend")))
  )
  private[this] lazy val controllers_HomeController_addFriend6_invoker = createInvoker(
    HomeController_0.addFriend(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "addFriend",
      Seq(classOf[String], classOf[String]),
      "POST",
      this.prefix + """friend/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """fireBaseIdFriend<[^/]+>/addFriend""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_HomeController_pushPunishIntoListG7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("punish/"), DynamicPart("firebaseIdOwn", """[^/]+""",true), StaticPart("/"), DynamicPart("firebaseIdOther", """[^/]+""",true), StaticPart("/"), DynamicPart("punishment", """[^/]+""",true), StaticPart("/pushPunishIntoListG")))
  )
  private[this] lazy val controllers_HomeController_pushPunishIntoListG7_invoker = createInvoker(
    HomeController_0.pushPunishIntoListG(fakeValue[String], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "pushPunishIntoListG",
      Seq(classOf[String], classOf[String], classOf[String]),
      "POST",
      this.prefix + """punish/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """firebaseIdOther<[^/]+>/""" + "$" + """punishment<[^/]+>/pushPunishIntoListG""",
      """ friendsPunishments""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_HomeController_getGetPunishList8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("punish/"), DynamicPart("firebaseId", """[^/]+""",true), StaticPart("/getGetPunishList")))
  )
  private[this] lazy val controllers_HomeController_getGetPunishList8_invoker = createInvoker(
    HomeController_0.getGetPunishList(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getGetPunishList",
      Seq(classOf[String]),
      "GET",
      this.prefix + """punish/""" + "$" + """firebaseId<[^/]+>/getGetPunishList""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_HomeController_getOwerPunishList9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("punish/"), DynamicPart("firebaseId", """[^/]+""",true), StaticPart("/getOwerPunishList")))
  )
  private[this] lazy val controllers_HomeController_getOwerPunishList9_invoker = createInvoker(
    HomeController_0.getOwerPunishList(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getOwerPunishList",
      Seq(classOf[String]),
      "GET",
      this.prefix + """punish/""" + "$" + """firebaseId<[^/]+>/getOwerPunishList""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_HomeController_pullPunishFromList10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("punish/"), DynamicPart("firebaseIdOwn", """[^/]+""",true), StaticPart("/"), DynamicPart("firebaseIdOther", """[^/]+""",true), StaticPart("/"), DynamicPart("punishment", """[^/]+""",true), StaticPart("/pullPunishFromList")))
  )
  private[this] lazy val controllers_HomeController_pullPunishFromList10_invoker = createInvoker(
    HomeController_0.pullPunishFromList(fakeValue[String], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "pullPunishFromList",
      Seq(classOf[String], classOf[String], classOf[String]),
      "POST",
      this.prefix + """punish/""" + "$" + """firebaseIdOwn<[^/]+>/""" + "$" + """firebaseIdOther<[^/]+>/""" + "$" + """punishment<[^/]+>/pullPunishFromList""",
      """""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_HomeController_updateUserLocation11_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("position/"), DynamicPart("firebaseId", """[^/]+""",true), StaticPart("/"), DynamicPart("longitude", """[^/]+""",true), StaticPart("/"), DynamicPart("latitude", """[^/]+""",true), StaticPart("/update")))
  )
  private[this] lazy val controllers_HomeController_updateUserLocation11_invoker = createInvoker(
    HomeController_0.updateUserLocation(fakeValue[String], fakeValue[Double], fakeValue[Double]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "updateUserLocation",
      Seq(classOf[String], classOf[Double], classOf[Double]),
      "POST",
      this.prefix + """position/""" + "$" + """firebaseId<[^/]+>/""" + "$" + """longitude<[^/]+>/""" + "$" + """latitude<[^/]+>/update""",
      """ Positioning""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_HomeController_getPostionOfUser12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("position/"), DynamicPart("firebaseId", """[^/]+""",true), StaticPart("/getPostionOfUser")))
  )
  private[this] lazy val controllers_HomeController_getPostionOfUser12_invoker = createInvoker(
    HomeController_0.getPostionOfUser(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getPostionOfUser",
      Seq(classOf[String]),
      "GET",
      this.prefix + """position/""" + "$" + """firebaseId<[^/]+>/getPostionOfUser""",
      """""",
      Seq()
    )
  )

  // @LINE:31
  private[this] lazy val controllers_HomeController_getPostionOfFriends13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("position/getPostionOfFriends")))
  )
  private[this] lazy val controllers_HomeController_getPostionOfFriends13_invoker = createInvoker(
    HomeController_0.getPostionOfFriends(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getPostionOfFriends",
      Nil,
      "GET",
      this.prefix + """position/getPostionOfFriends""",
      """""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_HomeController_updateEvents14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("event/"), DynamicPart("name", """[^/]+""",true), StaticPart("/"), DynamicPart("longitude", """[^/]+""",true), StaticPart("/"), DynamicPart("latitude", """[^/]+""",true), StaticPart("/"), DynamicPart("strafe1", """[^/]+""",true), StaticPart("/"), DynamicPart("strafe2", """[^/]+""",true), StaticPart("/update")))
  )
  private[this] lazy val controllers_HomeController_updateEvents14_invoker = createInvoker(
    HomeController_0.updateEvents(fakeValue[String], fakeValue[Double], fakeValue[Double], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "updateEvents",
      Seq(classOf[String], classOf[Double], classOf[Double], classOf[String], classOf[String]),
      "POST",
      this.prefix + """event/""" + "$" + """name<[^/]+>/""" + "$" + """longitude<[^/]+>/""" + "$" + """latitude<[^/]+>/""" + "$" + """strafe1<[^/]+>/""" + "$" + """strafe2<[^/]+>/update""",
      """ EventLocations""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index)
      }
  
    // @LINE:9
    case controllers_Assets_versioned1_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned1_invoker.call(Assets_1.versioned(path, file))
      }
  
    // @LINE:10
    case controllers_HomeController_test2_route(params@_) =>
      call { 
        controllers_HomeController_test2_invoker.call(HomeController_0.test())
      }
  
    // @LINE:14
    case controllers_HomeController_dbInit3_route(params@_) =>
      call { 
        controllers_HomeController_dbInit3_invoker.call(HomeController_0.dbInit())
      }
  
    // @LINE:17
    case controllers_HomeController_checkID4_route(params@_) =>
      call(params.fromPath[String]("firebaseId", None)) { (firebaseId) =>
        controllers_HomeController_checkID4_invoker.call(HomeController_0.checkID(firebaseId))
      }
  
    // @LINE:18
    case controllers_HomeController_checkIfFriend5_route(params@_) =>
      call(params.fromPath[String]("firebaseIdOwn", None), params.fromPath[String]("fireBaseIdFriend", None)) { (firebaseIdOwn, fireBaseIdFriend) =>
        controllers_HomeController_checkIfFriend5_invoker.call(HomeController_0.checkIfFriend(firebaseIdOwn, fireBaseIdFriend))
      }
  
    // @LINE:19
    case controllers_HomeController_addFriend6_route(params@_) =>
      call(params.fromPath[String]("firebaseIdOwn", None), params.fromPath[String]("fireBaseIdFriend", None)) { (firebaseIdOwn, fireBaseIdFriend) =>
        controllers_HomeController_addFriend6_invoker.call(HomeController_0.addFriend(firebaseIdOwn, fireBaseIdFriend))
      }
  
    // @LINE:22
    case controllers_HomeController_pushPunishIntoListG7_route(params@_) =>
      call(params.fromPath[String]("firebaseIdOwn", None), params.fromPath[String]("firebaseIdOther", None), params.fromPath[String]("punishment", None)) { (firebaseIdOwn, firebaseIdOther, punishment) =>
        controllers_HomeController_pushPunishIntoListG7_invoker.call(HomeController_0.pushPunishIntoListG(firebaseIdOwn, firebaseIdOther, punishment))
      }
  
    // @LINE:23
    case controllers_HomeController_getGetPunishList8_route(params@_) =>
      call(params.fromPath[String]("firebaseId", None)) { (firebaseId) =>
        controllers_HomeController_getGetPunishList8_invoker.call(HomeController_0.getGetPunishList(firebaseId))
      }
  
    // @LINE:24
    case controllers_HomeController_getOwerPunishList9_route(params@_) =>
      call(params.fromPath[String]("firebaseId", None)) { (firebaseId) =>
        controllers_HomeController_getOwerPunishList9_invoker.call(HomeController_0.getOwerPunishList(firebaseId))
      }
  
    // @LINE:25
    case controllers_HomeController_pullPunishFromList10_route(params@_) =>
      call(params.fromPath[String]("firebaseIdOwn", None), params.fromPath[String]("firebaseIdOther", None), params.fromPath[String]("punishment", None)) { (firebaseIdOwn, firebaseIdOther, punishment) =>
        controllers_HomeController_pullPunishFromList10_invoker.call(HomeController_0.pullPunishFromList(firebaseIdOwn, firebaseIdOther, punishment))
      }
  
    // @LINE:29
    case controllers_HomeController_updateUserLocation11_route(params@_) =>
      call(params.fromPath[String]("firebaseId", None), params.fromPath[Double]("longitude", None), params.fromPath[Double]("latitude", None)) { (firebaseId, longitude, latitude) =>
        controllers_HomeController_updateUserLocation11_invoker.call(HomeController_0.updateUserLocation(firebaseId, longitude, latitude))
      }
  
    // @LINE:30
    case controllers_HomeController_getPostionOfUser12_route(params@_) =>
      call(params.fromPath[String]("firebaseId", None)) { (firebaseId) =>
        controllers_HomeController_getPostionOfUser12_invoker.call(HomeController_0.getPostionOfUser(firebaseId))
      }
  
    // @LINE:31
    case controllers_HomeController_getPostionOfFriends13_route(params@_) =>
      call { 
        controllers_HomeController_getPostionOfFriends13_invoker.call(HomeController_0.getPostionOfFriends())
      }
  
    // @LINE:34
    case controllers_HomeController_updateEvents14_route(params@_) =>
      call(params.fromPath[String]("name", None), params.fromPath[Double]("longitude", None), params.fromPath[Double]("latitude", None), params.fromPath[String]("strafe1", None), params.fromPath[String]("strafe2", None)) { (name, longitude, latitude, strafe1, strafe2) =>
        controllers_HomeController_updateEvents14_invoker.call(HomeController_0.updateEvents(name, longitude, latitude, strafe1, strafe2))
      }
  }
}
