
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/HOME/TUM privat/SocialGameInstGuide/Assignment2_Server/conf/routes
// @DATE:Sun Jul 01 23:11:43 CEST 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def updateUserLocation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.updateUserLocation",
      """
        function(firebaseId0,longitude1,latitude2) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "position/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseId", firebaseId0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Double]].javascriptUnbind + """)("longitude", longitude1)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Double]].javascriptUnbind + """)("latitude", latitude2)) + "/update"})
        }
      """
    )
  
    // @LINE:25
    def pullPunishFromList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.pullPunishFromList",
      """
        function(firebaseIdOwn0,firebaseIdOther1,punishment2) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "punish/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseIdOwn", firebaseIdOwn0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseIdOther", firebaseIdOther1)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("punishment", punishment2)) + "/pullPunishFromList"})
        }
      """
    )
  
    // @LINE:31
    def getPostionOfFriends: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getPostionOfFriends",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "position/getPostionOfFriends"})
        }
      """
    )
  
    // @LINE:23
    def getGetPunishList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getGetPunishList",
      """
        function(firebaseId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "punish/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseId", firebaseId0)) + "/getGetPunishList"})
        }
      """
    )
  
    // @LINE:17
    def checkID: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.checkID",
      """
        function(firebaseId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "friend/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseId", firebaseId0)) + "/checkID"})
        }
      """
    )
  
    // @LINE:30
    def getPostionOfUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getPostionOfUser",
      """
        function(firebaseId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "position/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseId", firebaseId0)) + "/getPostionOfUser"})
        }
      """
    )
  
    // @LINE:14
    def dbInit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.dbInit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "init/init"})
        }
      """
    )
  
    // @LINE:19
    def addFriend: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.addFriend",
      """
        function(firebaseIdOwn0,fireBaseIdFriend1) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "friend/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseIdOwn", firebaseIdOwn0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("fireBaseIdFriend", fireBaseIdFriend1)) + "/addFriend"})
        }
      """
    )
  
    // @LINE:18
    def checkIfFriend: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.checkIfFriend",
      """
        function(firebaseIdOwn0,fireBaseIdFriend1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "friend/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseIdOwn", firebaseIdOwn0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("fireBaseIdFriend", fireBaseIdFriend1)) + "/checkIfFriend"})
        }
      """
    )
  
    // @LINE:34
    def updateEvents: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.updateEvents",
      """
        function(name0,longitude1,latitude2,strafe13,strafe24) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "event/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("name", name0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Double]].javascriptUnbind + """)("longitude", longitude1)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Double]].javascriptUnbind + """)("latitude", latitude2)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("strafe1", strafe13)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("strafe2", strafe24)) + "/update"})
        }
      """
    )
  
    // @LINE:24
    def getOwerPunishList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getOwerPunishList",
      """
        function(firebaseId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "punish/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseId", firebaseId0)) + "/getOwerPunishList"})
        }
      """
    )
  
    // @LINE:22
    def pushPunishIntoListG: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.pushPunishIntoListG",
      """
        function(firebaseIdOwn0,firebaseIdOther1,punishment2) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "punish/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseIdOwn", firebaseIdOwn0)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("firebaseIdOther", firebaseIdOther1)) + "/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("punishment", punishment2)) + "/pushPunishIntoListG"})
        }
      """
    )
  
    // @LINE:10
    def test: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.test",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "test"})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:9
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }


}
